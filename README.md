# jihu-hands-on

For jihu hans-on test.

## 文件介绍

###     cicdfiles：

​     用于保存ci/cd相关文件，因为其中helmcd是我自己封装的镜像在我的私有仓库中，所以这个deploy方式不适用于你们提供的TKE。我只是在自己的私有环境中完成了ci cd相关流程。已确>认可以正常构建镜像和发布应用。

###     terraform：

​    用于保存创建TKE的terraform配置文件

​	因为我平时使用terraform和TKE都比较少，所以这里时直接从官方文档中粘贴过来做了少量修改，参考链接：https://registry.terraform.io/providers/tencentcloudstack/tencentcloud/latest/docs/resources/eks_cluster

​	这里也没有真正的去创建过TKE集群，但是执行`terraform plan`返回是正常的

###     helmcharts：

​    部署service-a的helm chart

​	这里不是用ci/cd进行的部署，而是直接手动执行helm命令部署到TKE集群的。	
