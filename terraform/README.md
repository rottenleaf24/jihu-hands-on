## Usage

### 设置密钥：

```js
export TENCENTCLOUD_SECRET_ID="my-secret-id" 
export TENCENTCLOUD_SECRET_KEY="my-secret-key" 
export TENCENTCLOUD_REGION="ap-guangzhou" 
```

### 初始化：

`terraform init`

### 检测：

`terraform plan`

### 执行：

`terraform apply`
